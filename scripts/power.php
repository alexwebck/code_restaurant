<?php
/**
* Script calculates the power of a number x to index y without multiplication.
*/
$x = 7;
$power = 3;
$res = $x;

function increase($res, $number) {
  $transition = $res;
  
  for($i = 1; $i < $number; $i++) {
	$transition += $res;
  }
  
  return $transition;
}

for($i = 1; $i < $power; $i++) {
  $res = increase($res, $x);
}

echo $x ." to the power of " . $power . " = " . $res;