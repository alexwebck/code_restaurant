<?php
/**
* Script calculates and print 10 numbers for fibonacci series (with and without recursion).
*/

function factorial_recursive($number) {
    if ($number < 2) {
        return 1;
    } else {
        return ($number * factorial_recursive($number-1));
    }
}

echo "Factorial with recursion : factorial_recursive(10) = " . factorial_recursive(10) . "</br>";

function factorial($number) {
    $result = 1;
    while ($number > 0) {
        $result *= $number;
        $number--;
	}
    return $result;
}

echo "Factorial without recursion : factorial(10) = " . factorial(10);