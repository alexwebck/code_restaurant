<?php
/**
* Script finds the sum of all the multiples of two different numbers below 1000.
*/
$multiples = array();

// Uncomment the lines below to enter dividend manually from command line
//echo "Enter the dividend : \n"; 
//$dividend = trim(fgets(STDIN)); 

$dividend = 1000;

echo "Enter the first number : \n"; 
$firstNumber = trim(fgets(STDIN)); 

echo "Enter the second number : \n"; 
$secondNumber = trim(fgets(STDIN));

function isMultiple($dividend, $denominator) {
	return $dividend % $denominator == 0;
}

$start = microtime(true);
for ($i = 1; $i < $dividend; $i++) {
	if (isMultiple($i, $firstNumber) || isMultiple($i, $secondNumber)) {
		$multiples[] = $i;
	}
}

echo 'Sum of all the multiples : ' . array_sum($multiples);
echo "\nExecution time : " . (microtime(true) - $start) . " sec.";
