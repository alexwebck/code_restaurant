<?php
namespace Tinymvc\Application\Core;
/**
*	Router class performs MVC routing
*/
class Route
{
	static function start()
	{
		// setting controller and default action
		$controllerName = 'Main';
		$actionName = 'index';
		
		$routes = explode('/', $_SERVER['REQUEST_URI']);
		
		// getting controller name
		if ( !empty($routes[1]) )
		{	
			$controllerName = $routes[1];
		}
			
		// getting action name
		if ( !empty($routes[2]) && !is_numeric($routes[2]) )
		{
			$actionName = $routes[2];
		}

		// adding prefixes
		$modelName = 'Model'.ucfirst($controllerName);
		$controllerName = 'Controller'.ucfirst($controllerName);
		$actionName = 'action'.ucfirst($actionName);

		// trying to connect up model if it exists
		$modelFile = strtolower($modelName).'.php';
		$modelPath = "Application/Models/".$modelFile;
		if(file_exists($modelPath))
		{
			include "Application/Models/".$modelFile;
		}

		// trying to connect up controller class 
		$controllerFile = $controllerName.'.php';
		$controller_path = "Application/Controllers/".$controllerFile;
		if(file_exists($controller_path))
		{
			include "Application/Controllers/".$controllerFile;
			if(class_exists($controllerName)) {
				// instantiate controller
				$controller = new $controllerName;
				$action = $actionName;
				if(method_exists($controller, $action))
				{
					// call controller action
					$controller->$action();
				}
			}
		}
		else
		{	
			Route::ErrorPage404();
			throw new \Exception('Controller does not exist');
		}
	}

	function ErrorPage404()
	{
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
    }
    
}
