<?php
namespace Tinymvc\Application\Core;
/**
*	Base View class. Must be extended by custom controllers
*/
class View
{
	
	//public $template_view; // general template could be set here.
	
	/*
	$contentView - page view;
	$templateView - general page template;
	$data - data content array.
	*/
	function generate($contentView, $templateView, $data = null)
	{	
		//connect template
		include 'Application/Views/'.$templateView;
	}
}
