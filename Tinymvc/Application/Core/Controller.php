<?php
namespace Tinymvc\Application\Core;
/**
*	Base Controller class. Must be extended by custom controllers
*/
class Controller {
	
	public $model;
	public $view;
	
	function __construct()
	{
		$this->view = new View();
	}
	
	// default action
	function actionIndex()
	{
		// todo	
	}
}
