<?php
use Tinymvc\Application\Core\Controller;
use Tinymvc\Application\Core\View;

class ControllerMain extends Controller
{
	function actionIndex()
	{	
		$this->view->generate('MainView.php', 'TemplateView.php');
	}
}