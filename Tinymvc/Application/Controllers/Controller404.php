<?php
use Tinymvc\Application\Core\Controller;
use Tinymvc\Application\Core\View;

class Controller404 extends Controller
{
	function actionIndex()
	{
		$this->view->generate('404View.php', 'TemplateView.php');
	}
}
