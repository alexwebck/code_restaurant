<?php
use Tinymvc\Application\Core\Controller;
use Tinymvc\Application\Core\View;

class ControllerApi extends Controller
{

	function __construct()
	{
		$this->view = new View();
	}
	
	function actionIndex()
	{	
		$this->view->generate('APIView.php', 'TemplateView.php');
	}
}
