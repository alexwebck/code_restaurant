<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 3.0 License

Name       : Accumen
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20120712

Modified by Oleksandr Vlasenko
-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>Simple MVC Framework</title>
		<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/css?family=Kreon" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="/css/style.css" />
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="logo">
					<a href="/">MVC</span> <span class="cms">FRAMEWORK</span></a>
				</div>
				<div id="menu">
					<ul>
						<li class="first active"><a href="/">Main</a></li>
						<li><a href="/api">API</a></li>
						<li class="last"><a href="/contacts">Contacts</a></li>
					</ul>
					<br class="clearfix" />
				</div>
			</div>
			<div id="page">
				<div id="sidebar">
					<div class="side-box">
						<h3>Main menu</h3>
						<ul class="list">
							<li class="first "><a href="/">Main</a></li>
							<li><a href="/API">API</a></li>
							<li class="last"><a href="/contacts">Contacts</a></li>
						</ul>
					</div>
				</div>
				<div id="content">
					<div class="box">
						<?php include 'application/views/'.$contentView; ?>
					</div>
					<br class="clearfix" />
				</div>
				<br class="clearfix" />
			</div>
			<div id="page-bottom">
				<div id="page-bottom-sidebar">
					<h3>Contacts</h3>
					<ul class="list">
						<li class="first">skypeid: vlasenko_sasha</li>
						<li class="last">email: alexwebck@gmail.com</li>
					</ul>
				</div>
				<!--div id="page-bottom-content">
					<h3>About me</h3>
					<p>
					</p>
				</div-->
				<br class="clearfix" />
			</div>
		</div>
		<div id="footer">
			<a href="/">Oleksandr Vlasenko</a> &copy; 2014</a>
		</div>
	</body>
</html>