<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script>
(function() { 
	$.ajax({
		url: "/addressapi",
		dataType: 'json',
		success: function( data ) {
			var table = $("#results").find('tbody');
			for(i = 0; i < data.length; i++ ) {
				row = $('<tr/>');
				for(j = 0; j < data[i].length; j++ ) {
					row.append($('<td/>').text(data[i][j]));
				}
				table.append(row);
			}
		}
	});
})();
</script>

<h3>The response from http request</h3>

<table id="results">
	<tbody>
	</tbody>
</table>

