<h3>The API consists of the following methods:</h3>
<table border="1">
  <colgroup>
	<col width="9%" />
	<col width="30%" />
	<col width="62%" />
  </colgroup>
  <thead valign="bottom">
	<tr>
	  <th>Method</th>
	  <th>URL</th>
	  <th>Action</th>
	</tr>
  </thead>
  <tbody valign="top">
	<tr>
	  <td>GET</td>
	  <td>/addressapi</td>
	  <td>Retrieves all records</td>
	</tr>
	<tr>
	  <td>GET</td>
	  <td>/addressapi/2</td>
	  <td>Retrieves record based on row index number</td>
	</tr>
	<tr>
	  <td>POST</td>
	  <td>/addressapi</td>
	  <td>Adds a new record</td>
	</tr>
	<!--tr>
	  <td>PUT</td>
	  <td>/api/robots/2</td>
	  <td>Updates record based on row index numbery(is not impelemented)</td>
	</tr-->
	<tr>
	  <td>DELETE</td>
	  <td>/addressapi/2</td>
	  <td>Deletes record based on row index number</td>
	</tr>
  </tbody>
</table>