<?php
namespace Tinymvc\Application\Modules\CSV;

/**
 * Abstract base class that provides a common interface for the parser
 * along with shared methods and properties.
 */
 
abstract class CSVHandler {
	protected $maxLength = 0;
	protected $separator = '';
	public $csvData;
	
	public $headerArray = null;
	public $isFirstRowHeader = false;
	
	function __construct($maxLength = 1000, $separator = ',') {
		$this->maxLength = $maxLength;
		$this->separator = $separator;
		$this->csvData = $_SERVER['DOCUMENT_ROOT'].'/example.csv';
	}
	
	public function isEmpty() {
		return filesize($this->csvData) == 0;
	}
	
	public function getArrayFromCSV() {
		$array = null;
		
		if (($fh = fopen($this->csvData, "r"))) 
		{	
			while (($data = fgetcsv($fh, $this->maxLength, $this->separator))) {
				$array[] = $data;
			}

			fclose($fh);
		} else {
			throw new \Exception("Cannot parse data");
		}
		
		return $array;
	}
	
	public function parse($id = null) {
		$array = $this->getArrayFromCSV();
		return $this->processArray($array);
	}
	
	function findRow($row) {
		$array = $this->getArrayFromCSV();
		$result = false;
		for($i = 0; $i < count($array); $i++) {
			if ($row - 1 == $i) {
				$result = $array[$i];
				break;
			}
		}
		return !$result ? $result : $this->processArray($result);
	}
	
	public function addNewRow($array) {
		$file = fopen($this->csvData, 'a');
		fputcsv($file, $array);		
		fclose($file);
	}
	
	public function deleteRow($id) {
		$file = fopen($this->csvData, "r");
		$lineCount = 0;
		$new = '';
		while (!feof($file))
		{
			$line_of_text = fgetcsv($file, 1024);    
			if ($id - 1 != $lineCount++ && $line_of_text != null)
			{	
				$new .= implode(',', $line_of_text) . PHP_EOL;
			}
		}
		if($lineCount >= $id) 
		{
			file_put_contents($this->csvData, $new);
		}
		fclose($file);
		
		return $lineCount >= $id;
	}
	
	protected abstract function processArray($array);
}
