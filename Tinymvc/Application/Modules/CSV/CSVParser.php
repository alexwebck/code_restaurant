<?php
namespace Tinymvc\Application\Modules\CSV;

use Tinymvc\Application\Modules\CSV\CSVToJsonParser;

/**
 * Static Factory for creation of classes
 */
class CSVParser {

	public static function create($type, $maxLength = 1000, $separator = ',') {
		switch ($type) {
			case 'json':
				return new CSVToJsonParser($maxLength, $separator);
			//case 'xml':
			//	Xml parser can be implemented as well
		}
	}
}
