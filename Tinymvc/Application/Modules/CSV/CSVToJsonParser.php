<?php
namespace Tinymvc\Application\Modules\CSV;

use Tinymvc\Application\Modules\CSV\CSVHandler;

/**
 * Implementation to parse a csv document into json
 */
 
class CSVToJsonParser extends CSVHandler {
	
	protected function processArray($array) {
		
		if ($this->isFirstRowHeader) {
			$this->headerArray = $array[0];
			$array = $this->toAssocativeArray($array);
		}
		header('Content-Type: application/json');
		return json_encode($array);
	}
	
	private function toAssocativeArray($array) {

		$columnCount = count($array[0]);
		$temp = array();

		for ($i=1; $i < count($array); $i++) { 
			$item = array();
			
			for ($n=0; $n < $columnCount; $n++) { 
				$columnName = $this->headerArray[$n];
				$item[$columnName] = $array[$i][$n];	
			}

			$temp[] = $item;
			$item = null;
		}
		return $temp;
	}
}