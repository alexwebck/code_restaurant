<?php
namespace Tinymvc\Application\Modules\API;

use Tinymvc\Application\Modules\CSV\CSVToJsonParser;
use Tinymvc\Application\Modules\CSV\CSVParser;
use Tinymvc\Application\Modules\API\RestService;

/**
 * Implementation address API service
 */
class CSVAddressRestService extends RestService {
	
	private $type = "json";
	
	public function __construct($supportedMethods) {
		$this->supportedMethods = $supportedMethods;
	}
	
	protected function performGet($url, $arguments, $accept)
	{
		$CSVParser = CSVParser::create($this->type);
		
		//TODO implement the method to get specific record
		
		
		if($CSVParser->isEmpty()) {
            header('HTTP/1.1 404 Not Found');
		} else {
			if(isset($_GET['id']) && is_numeric($_GET['id'])) {
				$this->showSpecificRow($CSVParser);
			} else {
				print $CSVParser->parse();
				header('HTTP/1.1 200 OK');
			}
			
		}
	}
	
	private function showSpecificRow($CSVParser) {
		$id = $_GET['id'];
		$row = $CSVParser->findRow($id);
		if ($row) {
			print $row;
			header('HTTP/1.1 200 OK');
		} else {
			header('HTTP/1.1 500 The record with id = ' . $id . ' does not exist');
		}
	}
	
	protected function performPost($url, $arguments, $accept)
	{
		if(count($arguments) == 3) {
			$CSVParser = CSVParser::create($this->type);
			$CSVParser->addNewRow($arguments);
			header('HTTP/1.1 200 OK');
		} else {
			header('HTTP/1.1 400 Bad Request');
		}
	}
	
	protected function performPut($url, $arguments, $accept)
	{
		//TODO implement the method to update specific record
	}
	
	protected function performDelete($url, $arguments, $accept)
	{
		if(isset($_GET['id']) && is_numeric($_GET['id'])) {
			$id = $_GET['id'];
			$CSVParser = CSVParser::create($this->type);
			if ($CSVParser->deleteRow($id)) {
				header('HTTP/1.1 200 OK');
			} else {
				header('HTTP/1.1 500 The record with id = ' . $id . ' does not exist');
			}
		} else {
			header('HTTP/1.1 400 Error: Parameter id is missing or not correctly set');
		}
		
	}
}