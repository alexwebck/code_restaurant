<?php
use Tinymvc\Application\Core\Model;
use Tinymvc\Application\Modules\API\CSVAddressRestService;

/**
*	Model is used to perform Address API
*/

class ModelAddressApi extends Model
{	
	public function getData()
	{	
		$addressAPI = new CSVAddressRestService('GET,SET,PUT,DELETE');
		$addressAPI->handleRawRequest();
	}

}
