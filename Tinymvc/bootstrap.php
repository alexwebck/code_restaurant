<?php
require_once 'autoloader.php';

//connect core files
$classLoader = new SplClassLoader('Tinymvc\Application\Core', dirname($_SERVER['DOCUMENT_ROOT']));
$classLoader->register();

//connect modules files
$classLoader = new SplClassLoader('Tinymvc\Application\Modules', dirname($_SERVER['DOCUMENT_ROOT']));
$classLoader->register();

//call Router
Tinymvc\Application\Core\Route::start(); //call router
